FROM python:3.7-alpine

WORKDIR /app

ENV HOST=localhost
ENV MIN_SCORE=0
ENV WAIT=0

# install dependencies
RUN apk add git postgresql-dev gcc linux-headers musl-dev

# install http-observatory
RUN git clone https://github.com/mozilla/http-observatory.git /app
RUN pip3 install --upgrade .
RUN pip3 install --upgrade -r requirements.txt

# setup entrypoint
COPY entrypoint.sh entrypoint
RUN chmod +x entrypoint
ENTRYPOINT [ "/app/entrypoint" ]
