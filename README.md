# HTTP Observatory

> Docker image with [Mozilla HTTP Observatory](https://github.com/mozilla/http-observatory)

Environment variables:

- `HOST`: host to test (example: `connecteu.rs`)
- `MIN_SCORE`: required score to have at least, if the score is under this value, the script will fail (default: `0`)
- `WAIT`: time to sleep before running the scan, useful for waiting the deployment of the website to be done (default: `0`)

You can also pass arguments, but the `HOST` value will be ignored.
Please not change `--format` argument value or the script will fail.

## License

This program is free software and is distributed under [AGPLv3+ License](./LICENSE).
